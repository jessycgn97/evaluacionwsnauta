package ec.edu.ups.controlador;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.modelo.Carrito;
import ec.edu.ups.modelo.Producto;

@Stateless
public class DaoCarrito {
	
	@PersistenceContext(name="EvaluacionWSNautaPersistenceUnit")
	private EntityManager em;
	
	public void insertar(Carrito carrito) {
		em.persist(carrito);
	}
	
	public void actualizar(Carrito carrito) {
		em.merge(carrito);
	}

	public Carrito buscarCodigo(int id) {
		return em.find(Carrito.class, id);
	}

	public void eliminar(int id) {
		Carrito p = buscarCodigo(id);
			em.remove(p);
	}
	
	public List<Carrito> getCarrito() {
		String jpql = "SELECT c FROM Carrito c ";
		Query q = em.createQuery(jpql, Carrito.class);		
		return q.getResultList();
	}

}
