package ec.edu.ups.controlador;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.modelo.Cliente;
import ec.edu.ups.modelo.Producto;

@Stateless
public class DaoCliente {
	
	 @PersistenceContext(name="EvaluacionWSNautaPersistenceUnit")
		private EntityManager em;

	 public void insertar(Cliente producto) {
			em.persist(producto);
		}
		
		public void actualizar(Cliente producto) {
			em.merge(producto);
		}

		public Producto buscarCedula(String cedula) {
			return em.find(Producto.class, cedula);
		}

		public void eliminar(String cedula) {
			Producto p = buscarCedula(cedula);
				em.remove(p);
		}
		
		public List<Cliente> getCliente() {
			String jpql = "SELECT c FROM Cliente c ";
			Query q = em.createQuery(jpql, Cliente.class);		
			return q.getResultList();
		}

}
