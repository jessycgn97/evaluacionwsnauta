package ec.edu.ups.controlador;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.modelo.Producto;

@Stateless
public class DaoProducto {
	
	 @PersistenceContext(name="EvaluacionWSNautaPersistenceUnit")
		private EntityManager em;

	 	public void insertar(Producto producto) {
			em.persist(producto);
		}
	 	
	 	public void añadirPod(int id, int cantidad) {
	 		Producto p = buscarCodigo(id);
			em.persist(p);
		}
		
		public void actualizar(Producto producto) {
			em.merge(producto);
		}

		public Producto buscarCodigo(int id) {
			return em.find(Producto.class, id);
		}

		public void eliminar(int id) {
			Producto p = buscarCodigo(id);
				em.remove(p);
		}
		
		public List<Producto> getProducto() {
			String jpql = "SELECT p FROM Producto p ";
			Query q = em.createQuery(jpql, Producto.class);		
			return q.getResultList();
		}
		
		/*public List<Telefono> getTelefonos() {
			String jpql = "SELECT t FROM Telefono t";
			Query q = em.createQuery(jpql, Telefono.class);
			return q.getResultList();
		}*/

}
