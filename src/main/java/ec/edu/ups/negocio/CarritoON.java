package ec.edu.ups.negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.controlador.DaoCarrito;
import ec.edu.ups.modelo.Carrito;

@Stateless
public class CarritoON {
	
	@Inject
	public DaoCarrito daocarrito;
	
	@Inject
	public ProductoON productoon;
	
	public void guardarCarrito(Carrito carrito) {
		daocarrito.insertar(carrito);
	}
	
	public String guardarCarro(Carrito carrito) {
		try {
			Carrito car = new Carrito();
			car.setCantidad(carrito.getCantidad());
			car.setPrecio(carrito.getCantidad());
			car.setProducto(carrito.getProducto());
			productoon.actProducto(carrito.getProducto());
			daocarrito.insertar(car);
			return "carrito agregado";
		} catch (Exception e) {
			return "Error al agregar carrito";
		}
		
	}

}
