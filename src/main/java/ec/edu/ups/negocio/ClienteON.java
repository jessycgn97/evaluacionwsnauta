package ec.edu.ups.negocio;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.controlador.DaoCliente;
import ec.edu.ups.modelo.Cliente;

@Stateless
public class ClienteON {
	
	@Inject
	private DaoCliente daocliente;
	
	public void guardarCliente(Cliente cliente) {
		daocliente.insertar(cliente);
	}
	

}
