package ec.edu.ups.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.controlador.DaoProducto;
import ec.edu.ups.modelo.Producto;

@Stateless
public class ProductoON {
	
	@Inject
	private DaoProducto daoproducto;

	public ProductoON() {
	}
	
	public void guardarProducto(Producto producto) {
		daoproducto.insertar(producto);
	}
	
	public String guardarProd(Producto p) {
        try {
            Producto pro = new Producto();
            pro.setNombre(p.getNombre());
            pro.setCodigo(p.getCodigo());
            pro.setStock(p.getStock());
            pro.setPrecio(p.getPrecio());
            daoproducto.insertar(p);
            return "Producto Creado";
        } catch (Exception ex) {
            return "Error al crear Producto";
        }
    }
	
	public Producto buscarProducto(int id) throws Exception{
        try {
            return daoproducto.buscarCodigo(id);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
	
	public void añadirProducto(int id, int cantidad) {
		daoproducto.añadirPod(id, cantidad);
		
	}
	
	public List<Producto> listaProducto(){
		return daoproducto.getProducto();
	}
	
	 public void actProducto(Producto producto){
	        try {
	            producto.setStock(producto.getStock() - 1);
	            daoproducto.actualizar(producto);
	        } catch (Exception ex) {
	        }
	    }
	

}
