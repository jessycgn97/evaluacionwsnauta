package ec.edu.ups.services;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ec.edu.ups.negocio.ProductoON;

@Path("/carrito")
public class ServicioRest {
	
	@Inject
	private ProductoON productoon;
	
	@GET
	@Path("buscarID")
	@Produces("application/json")
	public void buscarid(@QueryParam("id") int id) throws Exception {
		productoon.buscarProducto(id);
	}
	
	@POST
	@Path("producto")
	@Produces("application/json")
	//@Consumes("application/json")
	public void añadirProducto(@QueryParam("id") int id, @QueryParam("cantidad") int cantidad) {
		productoon.añadirProducto(id, cantidad);
	}

}
